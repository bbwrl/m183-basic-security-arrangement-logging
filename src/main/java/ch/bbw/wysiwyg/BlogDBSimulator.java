package ch.bbw.wysiwyg;

import org.owasp.validator.html.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;

@Service
public class BlogDBSimulator {

    private List<BlogPost> blogPosts = new ArrayList<>();

    public List<BlogPost> getBlogPosts() {
        return blogPosts;
    }

    public void add(BlogPost blogPost) {
        try {
            Policy policy =  Policy.getInstance(
                    BlogDBSimulator.class.getResourceAsStream("/antisamy-tinymce.xml"));

            AntiSamy antiSamy = new AntiSamy();
            CleanResults cr = antiSamy.scan(blogPost.getTitle(), policy);

            blogPost.setTitle(cr.getCleanHTML());
            cr = antiSamy.scan(blogPost.getMessage(), policy);

            // TODO: add here the code for logging the errors if they occoure.
            // ...

        } catch (PolicyException e) {
            // TODO: add here the code for logging the exception
            // ...
            e.printStackTrace();
        } catch (ScanException e) {
            // TODO: add here the code for logging the exception
            // ...
            e.printStackTrace();
        }
    }
}
