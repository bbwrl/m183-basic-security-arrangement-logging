package ch.bbw.example;


import ch.bbw.wysiwyg.BlogDBSimulator;
import ch.bbw.wysiwyg.BlogPost;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
class BlogPostServiceTests {

    @Test
    void emptyTest() {
        BlogDBSimulator blogDBSimulator = new BlogDBSimulator();
        assertTrue(blogDBSimulator.getBlogPosts().size()==0);
    }

    @Test
    void addSimpleTest() {
        String title = "my title";
        String message = "my message";

        BlogDBSimulator blogDBSimulator = new BlogDBSimulator();
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(title);
        blogPost.setMessage(message);
        blogDBSimulator.add(blogPost);

        assertTrue(blogDBSimulator.getBlogPosts().size()==1);
        assertTrue(
            blogDBSimulator.getBlogPosts().get(0)
                .getTitle().equals(title));
        assertTrue(
            blogDBSimulator.getBlogPosts().get(0)
                .getMessage().equals(message));
    }

    @Test
    void addTestWithTags() {
        String title = "my Title";
        String message = "my <strong>bold</strong> message";

        BlogDBSimulator blogDBSimulator = new BlogDBSimulator();
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(title);
        blogPost.setMessage(message);
        blogDBSimulator.add(blogPost);

        assertTrue(blogDBSimulator.getBlogPosts().size()==1);
        assertTrue(
            blogDBSimulator.getBlogPosts().get(0)
                .getTitle().contains(title));

        assertTrue(
            blogDBSimulator.getBlogPosts().get(0)
                .getMessage().replaceAll("\n","").equals(message));
    }

    @Test
    void addTestWithCSSStyle() {
        String title = "my Title";
        String message = "my <span style='color:red'>red</span> message";

        BlogDBSimulator blogDBSimulator = new BlogDBSimulator();
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(title);
        blogPost.setMessage(message);
        blogDBSimulator.add(blogPost);

        assertTrue(
            blogDBSimulator.getBlogPosts().get(0)
                .getMessage().contains("style='color:red'"));
    }

    @Test
    void addTestWithScript() {
        String title = "my Title";
        String message = "my <script>alert('hack')</script> message";

        BlogDBSimulator blogDBSimulator = new BlogDBSimulator();
        BlogPost blogPost = new BlogPost();
        blogPost.setTitle(title);
        blogPost.setMessage(message);
        blogDBSimulator.add(blogPost);
        assertFalse(
            blogDBSimulator.getBlogPosts().get(0)
                .getMessage().contains("<script>alert('hack')</script>"));
    }

}
